import userUC from '../../use-cases/users/app';
import _ from '../../functions/app';
import userAdd from './insert-user';
import userLogin from './login-user';
// #####
const userAdds = userAdd(userUC.addUsers);
const userLogins = userLogin(userUC.loginUsers, _.dec, _.gToken);
// #####
const userController = {
    userAdds,
    userLogins
};

export default userController;
