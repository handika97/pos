const bcrypt = require('bcryptjs');

const userLogin = (loginUsers: Function, dec: Function, generateToken: Function) => {
    return async function get(httpRequest: any) {
        const headers = {
            'Content-Type': 'application/json'
        };
        try {
            //get the httprequest body
            const { source = {}, ...info } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers['User-Agent'];
            if (httpRequest.headers['Referer']) {
                source.referrer = httpRequest.headers['Referer'];
            }
            const dataRequest = {
                ...info,
                source
                // id: httpRequest.params.id // when id is passed
            };
            const view = await loginUsers(dataRequest);
            if (view.length > 0) {
                let isValid = dec(view[0].password);
                if (isValid == info.password) {
                    return {
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        statusCode: 200,
                        body: { data: { ...view[0], token: generateToken(view[0].password) } }
                    };
                } else {
                    return {
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        statusCode: 401,
                        body: { data: null, message: 'Password Was Wrong' }
                    };
                }
            }
            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 401,
                body: { data: null, message: 'User Not Found' }
            };
        } catch (e: any) {
            // TODO: Error logging
            console.log(e);
            return {
                headers,
                statusCode: 400,
                body: {
                    error: e.message
                }
            };
        }
    };
};

export default userLogin;
