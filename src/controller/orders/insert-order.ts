const orderAdd = (getOrderMenu: Function, getTrxCode: Function, insertTransaction: Function) => {
    return async function post(httpRequest: any) {
        try {
            const { source = {}, ...info } = httpRequest.body;
            source.ip = httpRequest.ip;
            source.browser = httpRequest.headers['User-Agent'];
            if (httpRequest.headers['Referer']) {
                source.referrer = httpRequest.headers['Referer'];
            }
            const posted = await getOrderMenu({
                ...info,
                source
            });

            let order_price = 0;
            let total_price = 0;
            let ppn_price = 0;

            info.order_list.forEach((item: any, i: number) => {
                let subTotalPrice =
                    posted.filter((postedItem: any, i: number) => {
                        return postedItem.menu_food_id === item.menu_food_id;
                    })[0]?.price * item.qty;
                order_price += subTotalPrice;
                info.order_list[i].sub_total_price = subTotalPrice;
            });
            ppn_price = (order_price * 10) / 100;
            total_price = order_price + ppn_price;

            const trx_code = await getTrxCode();
            const res = await insertTransaction({
                trx_code,
                payment_type_id: info.payment_type,
                service_type_id: info.service_type,
                ppn_price: ppn_price,
                order_price: order_price,
                total_price: total_price,
                order_list: info.order_list
            });

            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 201,
                body: { trx_code: trx_code, order_list: info.order_list, order_price }
            };
        } catch (e: any) {
            // TODO: Error logging
            console.log(e);

            return {
                headers: {
                    'Content-Type': 'application/json'
                },
                statusCode: 400,
                body: {
                    error: e.message
                }
            };
        }
    };
};

export default orderAdd;
