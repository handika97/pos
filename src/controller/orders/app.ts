import orderUC from '../../use-cases/orders/app';
import _ from '../../functions/app';
import getMenu from './get-menu';
import insertOrder from './insert-order';
import getReportMenu from './get-report-menu';
import getReportComposition from './get-report-composition';
import getReportPaid from './get-report-paid';
// #####
const getMenus = getMenu(orderUC.getMenus);
const insertOrders = insertOrder(orderUC.getOrderMenus, orderUC.getTrxCodes, orderUC.insertTransactions);
const getReportMenus = getReportMenu(orderUC.getReportMenus);
const getReportCompositions = getReportComposition(orderUC.getReportCompositions);
const getReportPaids = getReportPaid(orderUC.getReportPaids);
// #####
const orderController = {
    getMenus,
    insertOrders,
    getReportMenus,
    getReportCompositions,
    getReportPaids
};

export default orderController;
