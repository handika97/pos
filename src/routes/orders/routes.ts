import orderController from '../../controller/orders/app';
import _ from '../../functions/app';

const route = (router: any, makeExpressCallback: Function, validateAuth: Function) => {
    // GET
    router.get('/', makeExpressCallback(orderController.getMenus));
    router.get('/report/menu', makeExpressCallback(orderController.getReportMenus));
    router.get('/report/composition', makeExpressCallback(orderController.getReportCompositions));
    router.get('/report/paid', makeExpressCallback(orderController.getReportPaids));

    // POST
    router.post('/', makeExpressCallback(orderController.insertOrders));

    // PATCH

    // DELETE

    return router;
};

export default route;
