import userController from '../../controller/users/app';
import _ from '../../functions/app';

const route = (router: any, makeExpressCallback: Function, validateAuth: Function) => {
    // GET
    //login api
    router.post('/login', makeExpressCallback(userController.userLogins));

    // POST
    // add new user
    router.post('/', makeExpressCallback(userController.userAdds));

    // PATCH

    // DELETE

    return router;
};

export default route;
