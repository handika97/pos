const makeUser = () => {
    return function make(info: any) {
        const { service_type, payment_type, order_list } = info; // deconstruct
        if (!service_type) {
            throw new Error('Please enter service type.');
        }
        if (!payment_type) {
            throw new Error('Please enter payment method.');
        }
        if (!order_list) {
            throw new Error('Please select menu.');
        }

        order_list.forEach((item: { menu_food_id: Number; qty: Number }, i: Number) => {
            if (!item.menu_food_id && !item.qty) {
                throw new Error('Opppsss Something Wrong.');
            }
        });

        return Object.freeze({
            getServiceType: () => service_type,
            getPaymentType: () => payment_type,
            getOrderList: () => order_list
        });
    };
};

export default makeUser;
