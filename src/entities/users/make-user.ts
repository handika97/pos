const makeUser = (encrypt: Function) => {
    return function make(info: any) {
        const { first_name, last_name, email, username, phone_number, password } = info; // deconstruct
        if (!first_name) {
            throw new Error('Please enter first name.');
        }
        if (!last_name) {
            throw new Error('Please enter last name.');
        }
        if (!username) {
            throw new Error('Please enter username.');
        }
        if (!phone_number) {
            throw new Error('Please enter phone number.');
        }
        if (!email) {
            throw new Error('Please enter email.');
        }
        if (!password) {
            throw new Error('Please enter password.');
        }

        return Object.freeze({
            getFn: () => first_name,
            getLn: () => last_name,
            getEmail: () => email,
            getUsername: () => username,
            getPhoneNumber: () => phone_number,
            getPassword: () => encrypt(password)
        });
    };
};

export default makeUser;
