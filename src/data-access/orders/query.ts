import Sequelize, { Op, Transaction } from 'sequelize';

const query = (conn: any, models: any) => {
    return Object.freeze({
        getAllMenu,
        getOrderMenu,
        getTrxCode,
        insertTransaction,
        getReportMenu,
        getReportComposition,
        getReportPaid
    });

    async function getAllMenu(data: String) {
        try {
            const menuFood = models.menu_food;
            const foodPrice = models.food_price;
            const compositionFood = models.composition_food;
            const compositionType = models.composition_type;
            const res = await menuFood.findAll({
                include: [
                    {
                        model: foodPrice,
                        include: ['service_type', { model: compositionFood, include: { model: compositionType, include: 'uom', as: 'composition_type' }, as: 'composition_food' }],
                        as: 'food_price'
                    }
                ]
            });
            return res;
        } catch (e) {
            console.log('Error: ', e);
        }
    }

    async function getOrderMenu(data: { menu_food_id: Number[]; service_type_id: Number }) {
        try {
            const foodPrice = models.food_price;
            const res = await foodPrice.findAll({
                where: {
                    [Op.and]: [{ menu_food_id: [...data.menu_food_id] }, { service_type_id: data.service_type_id }]
                }
            });
            return res;
        } catch (e) {
            console.log('Error: ', e);
        }
    }

    async function getTrxCode(data: String) {
        try {
            const transaction = models.transaction;

            const res = await transaction.findOne({
                where: {
                    trx_code: {
                        [Op.like]: data + '%'
                    }
                },
                order: [['createdAt', 'DESC']]
            });
            return res;
        } catch (e) {
            console.log('Error: ', e);
        }
    }

    async function insertTransaction(data: any) {
        try {
            const transaction = models.transaction;
            const transaction_detail = models.transaction_detail;
            const res = await transaction.create(
                {
                    trx_code: data.trx_code,
                    service_type_id: data.service_type_id,
                    ppn_price: data.ppn_price,
                    order_price: data.order_price,
                    total_price: data.total_price,
                    payment_type_id: data.payment_type_id,
                    transaction_detail: data.order_list
                },
                { include: ['transaction_detail'] }
            );
            return res;
        } catch (e) {
            console.log('Error: ', e);
        }
    }

    async function getReportMenu(data: any) {
        try {
            const pool = await conn();

            const { service_type, start_date, end_date, length, start, search } = data; // deconstruct
            const res = await new Promise((resolve) => {
                const sortField: any = {
                    code: 'menu_foods.code',
                    name: ' menu_foods.name'
                };

                let sql = `SELECT transaction_details.id as id, transaction_details.created_at as date_transaction, menu_foods.code as food_code, menu_foods.name as food_name, service_types.name as service_type_name, transaction_details.sub_total_price as net_price, transaction_details.qty as qty, (transaction_details.sub_total_price * 10 / 100) + transaction_details.sub_total_price as gross_price
                FROM transaction_details JOIN transactions ON transaction_details.transaction_id = transactions.id 
                JOIN menu_foods on transaction_details.menu_food_id = menu_foods.id 
                JOIN service_types ON transactions.service_type_id = service_types.id where sub_total_price > 0`;
                let params: any = [];
                if (service_type) {
                    sql += ' and transactions.service_type_id = ?';
                    params = [service_type];
                }

                if (start_date) {
                    sql += " and CONVERT_TZ(transaction_details.created_at, '+00:00','+07:00') >= ?";
                    params = [...params, start_date];
                }

                if (end_date) {
                    sql += " and CONVERT_TZ(transaction_details.created_at, '+00:00','+07:00') <= ?";
                    params = [...params, end_date];
                }
                let countData = 0;

                if (search) {
                    sql += ' AND (';
                    Object.keys(sortField).forEach((d, i) => {
                        sql += ` ${i > 0 ? ' OR ' : ''}lower(${sortField[d]}) like ?`;
                        params = [...params, `%${search.toLowerCase()}%`];
                    });
                    sql += ' )';
                }

                pool.query(`SELECT count(*) as total from(${sql}) as dtCount`, params, (err: Error, result: any) => {
                    if (!err) {
                        countData = result[0].total;
                        sql += ' LIMIT ? OFFSET ?';
                        params = [...params, parseInt(length), parseInt(start)];

                        pool.query(sql, params, (err: Error, res: Response) => {
                            pool.end(); // end connection

                            if (err) resolve(err);
                            resolve({ data: res, count: countData });
                        });
                    }
                });
            });

            return res;
        } catch (e) {
            console.log('Error: ', e);
        }
    }

    async function getReportComposition(data: any) {
        try {
            const pool = await conn();
            const sortField: any = {
                code: 'composition_types.name',
                name: ' uoms.name'
            };
            const { start_date, end_date, length, start, search } = data; // deconstruct
            const res = await new Promise((resolve) => {
                let sql = `SELECT transaction_details.id as id, transaction_details.created_at as date_transaction, service_types.name as service_type_name, composition_foods.qty * transaction_details.qty as qty, food_prices.id as food_price_id, composition_types.name as name, uoms.name as uom
                FROM transaction_details JOIN transactions ON transaction_details.transaction_id = transactions.id 
                JOIN menu_foods on transaction_details.menu_food_id = menu_foods.id 
                JOIN service_types ON transactions.service_type_id = service_types.id 
                JOIN food_prices ON transactions.service_type_id = food_prices.service_type_id and transaction_details.menu_food_id = food_prices.menu_food_id
                JOIN composition_foods ON composition_foods.food_price_id = food_prices.id
                JOIN composition_types ON composition_foods.composition_type_id = composition_types.id
                JOIN uoms ON uoms.id = composition_types.uom_id
                where sub_total_price > 0`;
                let params: any = [];

                if (start_date) {
                    sql += " and CONVERT_TZ(transaction_details.created_at, '+00:00','+07:00') >= ?";
                    params = [...params, start_date];
                }

                if (end_date) {
                    sql += " and CONVERT_TZ(transaction_details.created_at, '+00:00','+07:00') <= ?";
                    params = [...params, end_date];
                }

                if (search) {
                    sql += ' AND (';
                    Object.keys(sortField).forEach((d, i) => {
                        sql += ` ${i > 0 ? ' OR ' : ''}lower(${sortField[d]}) like ?`;
                        params = [...params, `%${search.toLowerCase()}%`];
                    });
                    sql += ' )';
                }

                let countData = 0;

                pool.query(`SELECT count(*) as total from(${sql}) as dtCount`, params, (err: Error, result: any) => {
                    if (!err) {
                        countData = result[0].total;
                        sql += ' LIMIT ? OFFSET ?';
                        params = [...params, parseInt(length), parseInt(start)];

                        pool.query(sql, params, (err: Error, res: Response) => {
                            pool.end(); // end connection

                            if (err) resolve(err);
                            resolve({ data: res, count: countData });
                        });
                    }
                });
            });

            return res;
        } catch (e) {
            console.log('Error: ', e);
        }
    }

    async function getReportPaid(data: any) {
        try {
            const pool = await conn();
            const sortField: any = {
                name: 'payment_types.name'
            };
            const { start_date, end_date, length, start, search } = data; // deconstruct
            const res = await new Promise((resolve) => {
                let sql = `SELECT transaction_details.id as id, transaction_details.created_at as date_transaction, payment_types.name as payment_type, total_price
                FROM transaction_details JOIN transactions ON transaction_details.transaction_id = transactions.id 
                JOIN menu_foods on transaction_details.menu_food_id = menu_foods.id 
                JOIN service_types ON transactions.service_type_id = service_types.id 
                JOIN payment_types ON transactions.payment_type_id = payment_types.id 
                where sub_total_price > 0`;
                let params: any = [];

                if (start_date) {
                    sql += " and CONVERT_TZ(transaction_details.created_at, '+00:00','+07:00') >= ?";
                    params = [...params, start_date];
                }

                if (end_date) {
                    sql += " and CONVERT_TZ(transaction_details.created_at, '+00:00','+07:00') <= ?";
                    params = [...params, end_date];
                }

                if (search) {
                    sql += ' AND (';
                    Object.keys(sortField).forEach((d, i) => {
                        sql += ` ${i > 0 ? ' OR ' : ''}lower(${sortField[d]}) like ?`;
                        params = [...params, `%${search.toLowerCase()}%`];
                    });
                    sql += ' )';
                }

                let countData = 0;

                pool.query(`SELECT count(*) as total from(${sql}) as dtCount`, params, (err: Error, result: any) => {
                    if (!err) {
                        countData = result[0].total;
                        sql += ' LIMIT ? OFFSET ?';
                        params = [...params, parseInt(length), parseInt(start)];

                        pool.query(sql, params, (err: Error, res: Response) => {
                            pool.end(); // end connection

                            if (err) resolve(err);
                            resolve({ data: res, count: countData });
                        });
                    }
                });
            });

            return res;
        } catch (e) {
            console.log('Error: ', e);
        }
    }
};

export default query;
