import conn from '../app';
import models from '../sequelize/models/index';
// ######
import query from './query';
// ######
const orderDb = query(conn, models);
// ######
export default orderDb;
