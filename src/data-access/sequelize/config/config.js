require('dotenv').config();

module.exports = {
    development: {
        username: process.env.USER_DB,
        password: process.env.PASSWORD_DB,
        database: process.env.DATABASE,
        host: process.env.HOST_DB,
        port: process.env.PORT_DB,
        dialect: 'mysql',
        logging: false
    },
    test: {
        username: process.env.USER_DB,
        password: process.env.PASSWORD_DB,
        database: process.env.DATABASE_TEST,
        host: process.env.HOST_DB,
        port: process.env.PORT_DB,
        dialect: 'mysql',
        logging: false
    },
    production: {
        username: process.env.USER_DB,
        password: process.env.PASSWORD_DB,
        database: process.env.DATABASE,
        port: process.env.PORT_DB,
        host: process.env.HOST_DB,
        dialect: 'mysql'
    }
};
