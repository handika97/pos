'use strict';
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('transactions', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            trx_code: {
                type: Sequelize.STRING
            },
            service_type_id: {
                type: Sequelize.INTEGER
            },
            ppn_price: {
                type: Sequelize.INTEGER
            },
            ppn_price: {
                type: Sequelize.INTEGER
            },
            ppn_price: {
                type: Sequelize.INTEGER
            },
            payment_type_id: {
                type: Sequelize.INTEGER
            },
            created_at: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    async down(queryInterface, Sequelize) {
        await queryInterface.dropTable('transactions');
    }
};
