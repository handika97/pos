'use strict';
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('compositionFoods', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            food_price_id: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            composition_type_id: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            qty: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            created_at: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updated_at: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    async down(queryInterface, Sequelize) {
        await queryInterface.dropTable('composition_foods');
    }
};
