'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class menu_food extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    menu_food.init(
        {
            code: DataTypes.STRING,
            name: DataTypes.STRING,
            createdAt: {
                field: 'created_at',
                type: DataTypes.DATE
            },
            updatedAt: {
                field: 'updated_at',
                type: DataTypes.DATE
            }
        },
        {
            sequelize,
            modelName: 'menu_food'
        }
    );

    menu_food.associate = function (models) {
        menu_food.hasMany(models.food_price, { foreignKey: 'menu_food_id', as: 'food_price' });
    };
    return menu_food;
};
