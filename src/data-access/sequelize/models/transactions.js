'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class transaction extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    transaction.init(
        {
            trx_code: DataTypes.STRING,
            service_type_id: DataTypes.INTEGER,
            ppn_price: DataTypes.INTEGER,
            order_price: DataTypes.INTEGER,
            total_price: DataTypes.INTEGER,
            payment_type_id: DataTypes.INTEGER,
            createdAt: {
                field: 'created_at',
                type: DataTypes.DATE
            },
            updatedAt: {
                field: 'updated_at',
                type: DataTypes.DATE
            }
        },
        {
            sequelize,
            modelName: 'transaction'
        }
    );

    transaction.associate = function (models) {
        transaction.hasMany(models.transaction_detail, { foreignKey: 'transaction_id', as: 'transaction_detail' });
    };
    return transaction;
};
