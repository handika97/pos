'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class composition_food extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    composition_food.init(
        {
            food_price_id: DataTypes.INTEGER,
            composition_type_id: DataTypes.INTEGER,
            qty: DataTypes.INTEGER,
            createdAt: {
                field: 'created_at',
                type: DataTypes.DATE
            },
            updatedAt: {
                field: 'updated_at',
                type: DataTypes.DATE
            }
        },
        {
            sequelize,
            modelName: 'composition_food'
        }
    );

    composition_food.associate = function (models) {
        composition_food.belongsTo(models.composition_type, { foreignKey: 'composition_type_id', through: 'id', as: 'composition_type' });
    };
    return composition_food;
};
