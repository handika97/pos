'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class User extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    User.init(
        {
            first_name: DataTypes.STRING(255),
            last_name: DataTypes.STRING(255),
            phone_number: DataTypes.STRING(255),
            password: DataTypes.TEXT,
            username: DataTypes.STRING(255),
            email: DataTypes.STRING(255),
            token: DataTypes.TEXT,
            createdAt: {
                field: 'created_at',
                type: DataTypes.DATE
            },
            updatedAt: {
                field: 'updated_at',
                type: DataTypes.DATE
            }
        },
        {
            sequelize,
            modelName: 'user'
        }
    );

    return User;
};
