'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class composition_type extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    composition_type.init(
        {
            name: DataTypes.STRING,
            uom_id: DataTypes.INTEGER,
            createdAt: {
                field: 'created_at',
                type: DataTypes.DATE
            },
            updatedAt: {
                field: 'updated_at',
                type: DataTypes.DATE
            }
        },
        {
            sequelize,
            modelName: 'composition_type'
        }
    );

    composition_type.associate = function (models) {
        composition_type.belongsTo(models.uom, { foreignKey: 'uom_id', through: 'id', as: 'uom' });
    };
    return composition_type;
};
