'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class service_type extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    service_type.init(
        {
            name: DataTypes.STRING,
            createdAt: {
                field: 'created_at',
                type: DataTypes.DATE
            },
            updatedAt: {
                field: 'updated_at',
                type: DataTypes.DATE
            }
        },
        {
            sequelize,
            modelName: 'service_type'
        }
    );
    return service_type;
};
