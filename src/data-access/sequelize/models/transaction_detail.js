'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class transaction_detail extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    transaction_detail.init(
        {
            transaction_id: DataTypes.INTEGER,
            menu_food_id: DataTypes.INTEGER,
            qty: DataTypes.INTEGER,
            sub_total_price: DataTypes.INTEGER,
            createdAt: {
                field: 'created_at',
                type: DataTypes.DATE
            },
            updatedAt: {
                field: 'updated_at',
                type: DataTypes.DATE
            }
        },
        {
            sequelize,
            modelName: 'transaction_detail'
        }
    );
    return transaction_detail;
};
