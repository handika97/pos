'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class food_price extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    food_price.init(
        {
            menu_food_id: DataTypes.INTEGER,
            service_type_id: DataTypes.INTEGER,
            price: DataTypes.INTEGER,
            createdAt: {
                field: 'created_at',
                type: DataTypes.DATE
            },
            updatedAt: {
                field: 'updated_at',
                type: DataTypes.DATE
            }
        },
        {
            sequelize,
            modelName: 'food_price'
        }
    );

    food_price.associate = function (models) {
        food_price.belongsTo(models.service_type, { foreignKey: 'service_type_id', through: 'id', as: 'service_type' });
        food_price.hasMany(models.composition_food, { foreignKey: 'food_price_id', as: 'composition_food' });
    };
    return food_price;
};
