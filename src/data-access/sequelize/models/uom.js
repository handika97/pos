'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class uom extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    uom.init(
        {
            name: DataTypes.STRING,
            createdAt: {
                field: 'created_at',
                type: DataTypes.DATE
            },
            updatedAt: {
                field: 'updated_at',
                type: DataTypes.DATE
            }
        },
        {
            sequelize,
            modelName: 'uom'
        }
    );
    return uom;
};
