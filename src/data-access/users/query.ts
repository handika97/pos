const { Op } = require('sequelize');

const query = (conn: any, models: any) => {
    return Object.freeze({
        userLogin,
        insertUser
    });

    async function userLogin(data: String) {
        try {
            const User = models.user;
            const res = await User.findAll({
                where: {
                    [Op.or]: [{ username: data }, { email: data }, { phone_number: data }]
                }
            });
            return res;
        } catch (e) {
            console.log('Error: ', e);
        }
    }

    async function insertUser(data: Object) {
        try {
            const User = models.user;
            const res = await User.create(data);
            return res;
        } catch (e) {
            console.log('Error: ', e);
        }
    }
};

export default query;
