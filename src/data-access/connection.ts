require("dotenv").config();

const connect = (dotenv: any, mysql: any) => {
    return async function conn() {

        let config = null;
        const env = process.env.NODE_ENV;
        if (env == `development` || env == `production`) {
            config = {
                user: process.env.USER_DB,
                database: process.env.DATABASE,
                password: process.env.PASSWORD_DB,
                port: process.env.PORT_DB,
                host: process.env.HOST_DB
            };
        }

        if (env == `test`) {
            config = {
                user: process.env.USER_DB,
                database: process.env.DATABASE_TEST,
                password: process.env.PASSWORD_DB,
                port: process.env.PORT_DB,
                host: process.env.HOST_DB
            };
        }

        const pool = new mysql.createPool(config);

        return pool;
    };
};

export default connect;
