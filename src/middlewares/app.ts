const jwt = require('jsonwebtoken');
const password = process.env.ENCRYPTION_KEY;

// ####
import myAuth from './basic-auth';
// ####
const validateAuth = myAuth(jwt, password);
// ####

export default validateAuth;
