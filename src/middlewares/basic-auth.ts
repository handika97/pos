const myAuth = (jwt: any, password: any) => {
    return async function auths(req: any, res: any, next: any) {
        try {
            jwt.verify(req.headers['token'], password);
            next();
        } catch (err) {
            res.sendStatus(403);
        }
    };
};
export default myAuth;
