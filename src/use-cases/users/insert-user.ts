const addUser = (makeUsers: Function, userDb: any) => {
    return async function post(info: Object) {
        let data = await makeUsers(info); // entity

        data = {
            first_name: data.getFn(),
            last_name: data.getLn(),
            email: data.getEmail(),
            username: data.getUsername(),
            phone_number: data.getPhoneNumber(),
            password: data.getPassword(),
            created_at: new Date(),
            update_at: new Date()
        };
        const res = await userDb.insertUser(data);
        let msg = `Error on inserting user, please try again.`;

        if (res) {
            msg = `User has been added successfully.`;
            return msg;
        } else {
            throw new Error(msg);
        }
    };
};

export default addUser;
