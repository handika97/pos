const loginUser = (userDb: any) => {
    return async function selectUserForLogin(info: any) {
        let data = [];
        const res = await userDb.userLogin(info.username);
        if (res) {
            const items = res;
            for (let i = 0; i < items.length; i++) {
                const e = items[i];
                data.push({
                    id: e.id,
                    firstName: e.first_name,
                    lastName: e.last_name,
                    email: e.email,
                    phoneNumber: e.phoneNumber,
                    username: e.username,
                    password: e.password
                });
            }
        }
        return data;
    };
};

export default loginUser;
