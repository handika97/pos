import entity from '../../entities/users/app';
import userDb from '../../data-access/users/app';
import _ from '../../functions/app';
import loginUser from './login-user';
import addUser from './insert-user';

const loginUsers = loginUser(userDb);
const addUsers = addUser(entity.makeUsers, userDb);

// user use case
const userUC = {
    addUsers,
    loginUsers
};

export default userUC;
