const insertTransaction = (orderDB: any) => {
    return async function post(info: Object) {
        const res = await orderDB.insertTransaction(info);
        let msg = `Error on inserting order, please try again.`;

        if (res) {
            msg = `Order has been added successfully.`;
            return msg;
        } else {
            throw new Error(msg);
        }
    };
};

export default insertTransaction;
