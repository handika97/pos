const getMenu = (orderDB: any) => {
    return async function getMenu(info: any) {
        let data = [];
        const res = await orderDB.getAllMenu();
        if (res) {
            // only when there is data returned
            const items = res;
            for (let i = 0; i < items.length; i++) {
                const e = items[i];

                // push items to array
                data.push(e);
            }
        }
        return data;
    };
};

export default getMenu;
