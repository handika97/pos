const getReportMenu = (orderDB: any) => {
    return async function getReportMenu(info: any) {
        let data = [];
        const res = await orderDB.getReportMenu(info);
        if (res.data) {
            // only when there is data returned
            const items = res.data;
            for (let i = 0; i < items.length; i++) {
                const e = items[i];

                // push items to array
                data.push(e);
            }
        }
        return { count: res.count, data };
    };
};

export default getReportMenu;
