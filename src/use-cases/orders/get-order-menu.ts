const getOrderMenu = (orderDB: any) => {
    return async function getMenu(info: any) {
        let data = {
            menu_food_id: info?.order_list.map((item: any) => item.menu_food_id),
            service_type_id: info.service_type
        };
        let response = [];
        const res = await orderDB.getOrderMenu(data);
        if (res) {
            // only when there is data returned
            const items = res;
            for (let i = 0; i < items.length; i++) {
                const e = items[i];

                // push items to array
                response.push(e);
            }
        }
        return response;
    };
};

export default getOrderMenu;
