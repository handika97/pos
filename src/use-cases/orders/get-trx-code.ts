const getTrxCode = (orderDB: any, moment: Function) => {
    return async function getTrxCode() {
        let response = [];
        const d = new Date();
        let num = 0;
        const res = await orderDB.getTrxCode(moment(d).format('DDMMYYYY'));
        if (res) {
            num = parseInt(res.trx_code.split('.')[1]) + 1;
        } else {
            num = 1;
        }
        return moment(d).format('DDMMYYYY') + '.' + num;
    };
};

export default getTrxCode;
