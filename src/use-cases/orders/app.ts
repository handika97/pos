import entity from '../../entities/users/app';
import orderDb from '../../data-access/orders/app';
import _ from '../../functions/app';
import getMenu from './get-menu';
import moment from 'moment';
import getOrderMenu from './get-order-menu';
import getTrxCode from './get-trx-code';
import insertTransaction from './insert-transaction';
import getReportMenu from './get-report-menu';
import getReportComposition from './get-report-composition';
import getReportPaid from './get-report-paid';

const getMenus = getMenu(orderDb);
const getOrderMenus = getOrderMenu(orderDb);
const getTrxCodes = getTrxCode(orderDb, moment);
const insertTransactions = insertTransaction(orderDb);
const getReportMenus = getReportMenu(orderDb);
const getReportCompositions = getReportComposition(orderDb);
const getReportPaids = getReportPaid(orderDb);

// user use case
const userUC = {
    getMenus,
    insertTransactions,
    getOrderMenus,
    getTrxCodes,
    getReportMenus,
    getReportCompositions,
    getReportPaids
};

export default userUC;
